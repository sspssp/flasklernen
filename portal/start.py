from portal import app
from flask import render_template

@app.route("/")
def index():
        name = "TestWebseite"
        inhalt = "GEHEIM"
        return render_template("index.html", name = name, inhalt = inhalt)
