from portal import app
# FLASK CONFIGURATION; PLEASE CHANGE FOR PRODUCTIVE USE; LOOK AT http://flask.pocoo.org/docs/config/#builtin-configuration-values
DEBUG = True

app.config.from_object(__name__)

# run the app
app.run()
